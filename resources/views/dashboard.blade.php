<x-app-layout>
    <x-slot name="header">
        <h2 class="h4 font-weight-bold">
            {{ __('Dashboard') }}
        </h2>
    </x-slot>
    <x-slot name="title">
        {{ __('Duo-Dashboard') }}
    </x-slot>

    <livewire:dashboards />
</x-app-layout>
