<x-slot name="header">
    <h2 class="h4 font-weight-bold">
        {{ __('Pos') }}
    </h2>
</x-slot>
<x-slot name="title">
    {{ __('Duo-Pos') }}
</x-slot>
<div>
    <!-- Current Projects -->
    <div class="row">
        <div class="col-md-8 mb-4 mb-md-0">
    <!-- Overall Income -->
    <div class="card mb-4">
        <!-- Card Header -->
        <header class="card-header d-md-flex align-items-center">
            <h2 class="h3 card-header-title">Products</h2>


        </header>
        <!-- End Card Header -->
        <!-- Card Body -->
        <div class="card-body">
            <!-- Doughnut Chart -->
            <div class="row">
                <div class="col-md-12">
                    <input type="text" wire:model="search" placeholder="Search..." class="from-control mb-3">
                </div>
                @foreach($products as $product)
                    <div class="col-md-3 mt-2">
                        <div class="card">
                            <div class="card-header d-md-flex align-items-center p-2 h5 text-uppercase text-muted ">{{ $product->name }}</div>
                        <img class="card-img-bottom" src="{{asset('assets/img/default.jpg')}}" alt="Card image cap">
                            <div class="card-footer">
                                <button wire:click="addToCart({{$product->id}})" class="btn btn-success">Add</button>
                            </div>
                        </div>
                    </div>
                @endforeach
                {{$products->links()}}



            </div>
            <!-- End Doughnut Chart -->
        </div>
        <!-- End Card Body -->
    </div>
    <!-- End Overall Income -->


        </div>

        <!-- Current Projects -->
        <div class="col-md-4 mb-4 mb-md-0">
            <div class="card h-100">
                <header class="card-header d-flex align-items-center">
                    <h2 class="h3 card-header-title">Carts ({{count($cart['products'])}})</h2>

                    <!-- Card Header Icon -->
                    <ul class="list-inline ml-auto mb-0">
                        <li class="list-inline-item mr-3">
                            <a class="link-muted h3" href="#!">
                                <i class="far fa-bell"></i>
                            </a>
                        </li>
                        <li class="list-inline-item">
                            <a class="link-muted h3" href="#!">
                                <i class="far fa-edit"></i>
                            </a>
                        </li>
                    </ul>
                    <!-- End Card Header Icon -->
                </header>

                <div class="card-body">
                     <div class="d-flex justify-content-between mb-3">
                        <div>
                            <span class="d-none d-lg-block text-muted small text-uppercase mb-1">Name</span>
                            <input class="h4 text-primary" name="name-user">
                        </div>

                        <div class="divider divider-vertical mx-2"></div>

                        <div>
                            <span class="d-none d-lg-block text-muted small text-uppercase mb-1">Type</span>
                            <select class="h4 text-info" name="type">
                                <option value="1">Grosiran</option>
                                <option value="2">Warung</option>
                                <option value="3">supermarket</option>
                            </select>
                        </div>

                    </div>

                    <div class="table-responsive">
                        <table class="table align-middle mb-0">
                            <thead class="table-active text-muted">
                                <tr class="small text-muted text-uppercase">
                                    <th>Name </th>
                                    <th>QTY</th>
                                    <th>Price</th>
                                </tr>
                            </thead>
                            <tbody>
                             @php $total=0;@endphp
                             @forelse($cart['products'] as  $key =>$productCart)
                                 <tr>
                                         <td class="align-middle">
                                             <div class="media align-items-center">
                                                 <img src="{{asset('storage/default.jpg')}}" alt="Card image cap" style="width: 50px;">
                                                 <div class="media-body">
                                                     <h4 class="mb-0">{{($productCart->name ?? '') }}</h4>
                                                 </div>
                                             </div>
                                         </td>
                                         <td class="align-middle font-weight-semibold">{{$key}}
                                         </td>
                                         <td class="align-middle font-weight-semibold">{{($productCart->sell_price ?? '') }}</td>
                                         <td class="align-middle font-weight-semibold"><button wire:click="removeItem({{($productCart->id ?? '1') }})" class="btn btn-danger">X</button></td>
{{--                                     @endif--}}


                                 </tr>
                             @php $total = $total+ intval($productCart->sell_price ?? 0) @endphp
                             @empty

                             @endforelse
                            </tbody>
                        </table>
                    </div>

                </div>

                <footer class="card-footer">
                    <a class="u-link u-link--primary" href="#!">Rp. {{$total}}</a>
                    @if($cart['products'])
                        <button wire:click="checkout" class="btn btn-success float-right">Checkout</button>
                    @endif
                </footer>
            </div>
        </div>
        <!-- End Current Projects -->

    </div>
    <!-- End Current Projects -->
</div>
@push('scripts')
<!-- Plugins -->
<script src="./assets/vendor/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min.js"></script>
<script src="./assets/vendor/chart.js/dist/Chart.min.js"></script>

<!-- Initialization  -->
<script src="./assets/js/sidebar-nav.js"></script>
<script src="./assets/js/main.js"></script>
<script src="./assets/js/dashboard-page-scripts.js"></script>
@endpush
