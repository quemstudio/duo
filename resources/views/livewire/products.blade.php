<x-slot name="header">
    <h2 class="h4 font-weight-bold">
        {{ __('Products Management') }}
    </h2>
</x-slot>
<x-slot name="title">
    {{ __('Duo - Product') }}
</x-slot>
<div>
    <div>
        @if (session()->has('message'))
            <div class="alert alert-success">
                {{ session('message') }}
            </div>
        @endif
    </div>
    <div class="card mb-4">
        <header class="card-header">
            <button type="button" class="btn btn-info btn-pill float-right" wire:click="create()"> <i class="fa fa-plus mr-2"></i>Add {{$tittle}}</button>
        </header>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th >#</th>
                            <th >SKU</th>
                            <th >Brand</th>
                            <th >Name</th>
                            {{-- <th >Buy Price</th> --}}
                            <th >Stock</th>
                            <th class="text-center">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($products as $produt)
                            <tr>
                                <td >{{ $produt->id }}</td>
                                <td >{{ $produt->sku }}</td>
                                <td >{{ $produt->brand }}</td>
                                <td >{{ $produt->name }}</td>
                                {{-- <td >{{ $produt->buy_price }}</td> --}}
                                <td >{{ $produt->total }}</td>
                                <td class="text-center">
                                    <button type="button" class="btn btn-warning btn-circle" wire:click="edit({{ $produt->id }})">
                                        <i class="fa fa-pencil-alt"></i>
                                    </button>
                                    <button type="button" class="btn btn-danger btn-circle" wire:click="delete({{ $produt->id }})">
                                      <i class="fa fa-trash"></i>
                                    </button>
                                    {{-- <a id="actions1Invoker" class="link-muted" href="#!" aria-haspopup="true" aria-expanded="false"
                                       data-toggle="dropdown">
                                        <i class="fa fa-sliders-h"></i>
                                    </a>
                                    <div class="dropdown-menu dropdown-menu-right dropdown" style="width: 150px;" aria-labelledby="actions1Invoker">
                                        <ul class="list-unstyled mb-0">
                                            <li>
                                                <a class="d-flex align-items-center link-muted py-2 px-3" wire:click="edit({{ $produt->id }})" href="#!">
                                                    <i class="fa fa-pencil-alt mr-2"></i> Edit
                                                </a>
                                            </li>
                                            <li>
                                                <a class="d-flex align-items-center link-muted py-2 px-3" wire:click="delete({{ $produt->id }})" href="#!">
                                                    <i class="fa fa-minus mr-2"></i> Remove
                                                </a>
                                            </li>
                                        </ul>
                                    </div> --}}
                                </td>
                            </tr>
                        @endforeach

                    </tbody>

                </table>
                  <div class="float-right">{{ $products->links() }}</div>

            </div>
        </div>
    </div>
    <!-- Modal -->
    <div wire:ignore.self class="modal fade" id="modals" tabindex="-1" role="dialog" aria-labelledby="edit-modal-label"
        aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h3 class="modal-title" id="edit-modal-label">{{$modalTitle}}
                    </h3>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form id="form_edit" method="post">
                    @csrf
                    <div class="modal-body">
                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label for="product_sku" class="form-label right">SKU</label>
                                <input type="text" id="product_sku" wire:model="sku" class="form-control">
                                @error('sku') <span class="text-danger">{{ $message }}</span>@enderror
                            </div>
                            <div class="col-md-6 mb-3">
                                <label for="category" class="form-label right">Category</label>
                                <div wire:ignore>
                                    <select class="select2 form-control" name="category" wire:model="category_id" style="width: 100%; height:  calc(2.40625rem + 2px) ">
                                        @foreach ($categories as $item)
                                        <option value="{{$item->id}}">{{$item->name}}</option>
                                        @endforeach
                                    </select>
                                    <!-- Select2 will insert its DOM here. -->
                                </div>
                                @error('category') <span class="text-danger">{{ $message }}</span>@enderror
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label for="product_name" class="form-label right">Name</label>
                                <input type="text" id="product_name" wire:model="name" class="form-control">
                                @error('name') <span class="text-danger">{{ $message }}</span>@enderror
                            </div>
                            <div class="col-md-6 mb-3">
                                <label for="product_brand" class="form-label right">Brand</label>
                                <input type="text" id="product_brand" wire:model="brand" class="form-control">
                                @error('brand') <span class="text-danger">{{ $message }}</span>@enderror
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label for="product_in_data" class="form-label right">In Date</label>
                                <input type="text" id="product_in_data" wire:model="in_date"
                                    class="form-control date-picker-input"
                                    onchange="this.dispatchEvent(new InputEvent('input'))">
                                @error('in_date') <span class="text-danger">{{ $message }}</span>@enderror
                            </div>
                            <div class="col-md-6 mb-3">
                                <label for="product_exp_date" class="form-label right">Expired Date</label>
                                <input type="text" id="product_exp_date" wire:model="exp_date"
                                    class="form-control date-picker-input"
                                    onchange="this.dispatchEvent(new InputEvent('input'))">
                                @error('exp_date') <span class="text-danger">{{ $message }}</span>@enderror
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label for="product_buy_price" class="form-label right">Buy Price (Rp)</label>
                                <input type="number" id="product_buy_price" wire:model="buy_price" class="form-control">
                                @error('buy_price') <span class="text-danger">{{ $message }}</span>@enderror
                            </div>
                            <div class="col-md-6 mb-3">
                                <label for="product_sell_price" class="form-label right">Sell Price (Rp)</label>
                                <input type="number" id="product_sell_price" wire:model="sell_price" class="form-control">
                                @error('sell_price') <span class="text-danger">{{ $message }}</span>@enderror
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="product_total" class="form-label right">Stock</label>
                            <input type="number" id="product_total" wire:model="total" class="form-control">
                            @error('total') <span class="text-danger">{{ $message }}</span>@enderror
                        </div>
                        <div class="form-group">
                            <label for="product_description" class="form-label right">Notes</label>
                            <input type="text" id="product_description" wire:model="description" class="form-control">
                            @error('description') <span class="text-danger">{{ $message }}</span>@enderror
                        </div>
                        <div class="form-group">
                            <label for="product_picture" class="form-label right">Picture</label>
                            <input type="file" wire:model="picture" id="product_picture">
                            @error('photo') <span class="error">{{ $message }}</span> @enderror
                        </div>


                    </div>
                    <div class="modal-footer">
                        <button wire:click.prevent="store()" type="button" class="btn btn-primary">
                            <span class="fa fa-download mr-1"></span>Save
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@push('modals')

@endpush
@push('styles')
    <link rel="stylesheet" type="text/css" href="{{asset('assets/vendor/select2/dist/css/select2.min.css')}} " />
    <link rel="stylesheet" type="text/css" href="{{asset('assets/vendor/select2-bootstrap4/dist/select2-bootstrap4.min.css')}} " />
    <link rel="stylesheet" type="text/css" href="./assets/vendor/bootstrap-datepicker/css/datepicker.css" />
@endpush
@push('scripts')
    <!-- Plugins -->
    <script src="./assets/vendor/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min.js"></script>
    <script type="text/javascript" src="assets/vendor/select2/dist/js/select2.full.min.js"></script>
    <script type="text/javascript" src="assets/vendor/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>

    <!-- Initialization  -->
    <script src="./assets/js/sidebar-nav.js"></script>
    <script src="./assets/js/main.js"></script>


    <script type="text/javascript">
        window.livewire.on('showModal', () => {
            $('#modals').modal('show');
        });
        window.livewire.on('closeModal', () => {
            $('#modals').modal('hide');
        });

        $(document).ready(function() {
            $('.select2').select2({
                theme: 'bootstrap4',
            });
            $('.date-picker-input').datepicker({
            format: 'yyyy-mm-dd',
            autoclose: true,
            orientation: "auto",
            clearBtn : true,
            todayHighlight: true,
            templates: {
                leftArrow: '<i class="fa fa-angle-left"></i>',
                rightArrow: '<i class="fa fa-angle-right"></i>'
            }
        });
        });
    </script>
@endpush
