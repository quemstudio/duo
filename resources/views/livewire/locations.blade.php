<x-slot name="header">
    <h1 class="h2 font-weight-bold">Locations Management</h1>
</x-slot>
<x-slot name="title">
    {{ __('Duo-Location') }}
</x-slot>
<div>
    <div>
        @if (session()->has('message'))
            <div class="alert alert-success">
                {{ session('message') }}
            </div>
        @endif
    </div>
    <div class="card mb-4">
        <header class="card-header">
            <button type="button" class="btn btn-sm btn-info float-right" wire:click="create()"> <i class="fa fa-plus mr-2"></i>Add {{$tittle}}</button>
        </header>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th >#</th>
                            <th >Name</th>
                            <th >City</th>
                            <th >Address</th>
                            <th class="text-center">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($locations as $location)
                            <tr>
                                <td >{{ $location->id }}</td>
                                <td >{{ $location->name }}</td>
                                <td >{{ $location->city_id }}</td>
                                <td >{{ $location->address }}</td>
                                <td class="text-center">
                                    <a id="actions1Invoker" class="link-muted" href="#!" aria-haspopup="true" aria-expanded="false"
                                       data-toggle="dropdown">
                                        <i class="fa fa-sliders-h"></i>
                                    </a>
                                    <div class="dropdown-menu dropdown-menu-right dropdown" style="width: 150px;" aria-labelledby="actions1Invoker">
                                        <ul class="list-unstyled mb-0">
                                            <li>
                                                <a class="d-flex align-items-center link-muted py-2 px-3" wire:click="edit({{ $location->id }})" href="#!">
                                                    <i class="fa fa-pencil-alt mr-2"></i> Edit
                                                </a>
                                            </li>
                                            <li>
                                                <a class="d-flex align-items-center link-muted py-2 px-3" wire:click="delete({{ $location->id }})" href="#!">
                                                    <i class="fa fa-minus mr-2"></i> Remove
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </td>
                            </tr>
                        @endforeach

                    </tbody>

                </table>
                  <div class="float-right">{{ $locations->links() }}</div>

            </div>
        </div>
    </div>
    <!-- Modal -->
    <div wire:ignore.self class="modal fade" id="modals" tabindex="-1" role="dialog" aria-labelledby="edit-modal-label"
        aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h3 class="modal-title" id="edit-modal-label">{{$modalTitle}}
                    </h3>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form id="form_edit" method="post">
                    @csrf
                    <div class="modal-body">
                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label for="name" class="form-label right">Name</label>
                                <input type="text" id="name" wire:model="name" class="form-control">
                                @error('name') <span class="text-danger">{{ $message }}</span>@enderror
                            </div>
                            <div class="col-md-6 mb-3">
                                <label for="address" class="form-label right">Address</label>
                                <input type="text" id="address" wire:model="address" class="form-control">
                                @error('address') <span class="text-danger">{{ $message }}</span>@enderror
                            </div>
                        </div>
                        {{-- <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label for="province_id" class="form-label right">Province</label>
                                <input type="text" id="province_id" wire:model="province_id" class="form-control">
                                @error('province_id') <span class="text-danger">{{ $message }}</span>@enderror
                            </div>
                            <div class="col-md-6 mb-3">
                                <label for="city_id" class="form-label right">City</label>
                                <input type="text" id="city_id" wire:model="city_id" class="form-control">
                                @error('city_id') <span class="text-danger">{{ $message }}</span>@enderror
                            </div>
                        </div> --}}
                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label for="lat" class="form-label right">Latitude</label>
                                <input type="text" id="lat" wire:model="lat" class="form-control">
                                @error('lat') <span class="text-danger">{{ $message }}</span>@enderror
                            </div>
                            <div class="col-md-6 mb-3">
                                <label for="lng" class="form-label right">Longitude</label>
                                <input type="text" id="lng" wire:model="lng" class="form-control">
                                @error('lng') <span class="text-danger">{{ $message }}</span>@enderror
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button wire:click.prevent="store()" type="button" class="btn btn-primary">
                            <span class="fa fa-download mr-1"></span>Save
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@push('modals')

@endpush
@push('styles')
    <link rel="stylesheet" type="text/css" href="./assets/vendor/bootstrap-datepicker/css/datepicker.css" />
@endpush
@push('scripts')
    <!-- Plugins -->
    <script src="./assets/vendor/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min.js"></script>
    <!-- Initialization  -->
    <script src="./assets/js/sidebar-nav.js"></script>
    <script src="./assets/js/main.js"></script>

    <script type="text/javascript" src="assets/vendor/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
    <script type="text/javascript">
        window.livewire.on('showModal', () => {
            $('#modals').modal('show');
        });
        window.livewire.on('closeModal', () => {
            $('#modals').modal('hide');
        });
        $('.date-picker-input').datepicker({
            format: 'yyyy-mm-dd',
            autoclose: true,
            orientation: "auto",
            clearBtn : true,
            todayHighlight: true,
            templates: {
                leftArrow: '<i class="fa fa-angle-left"></i>',
                rightArrow: '<i class="fa fa-angle-right"></i>'
            }
        });
    </script>
@endpush
