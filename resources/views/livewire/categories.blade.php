<x-slot name="header">
    <h1 class="h2 font-weight-bold">Category Management</h1>
</x-slot>
<x-slot name="title">
    {{ __('Duo-Category') }}
</x-slot>
<div>
    <div>
        @if (session()->has('message'))
            <div class="alert alert-success">
                {{ session('message') }}
            </div>
        @endif
    </div>
    <div class="card mb-4">
        <header class="card-header">
            <button type="button" class="btn  btn-info btn-pill float-right" wire:click="create()"> <i class="fa fa-plus mr-2"></i>Add {{$tittle}}</button>
        </header>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th >#</th>
                            <th >Name</th>
                            <th class="text-center">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($categories as $category)
                            <tr>
                                <td >{{ $category->id }}</td>
                                <td >{{ $category->name }}</td>
                                <td class="text-center">
                                    <button type="button" class="btn btn-warning btn-circle" wire:click="edit({{ $category->id }})">
                                        <i class="fa fa-pencil-alt"></i>
                                      </button>

                                    {{-- <div class="text-center">
                                        <a id="actions1Invoker" class="link-muted" href="#!" aria-haspopup="true" aria-expanded="false" data-toggle="dropdown">
                                            <i class="fa fa-sliders-h"></i>
                                        </a>
                                        <div class="dropdown-menu dropdown-menu-right dropdown" style="width: 150px;" aria-labelledby="actions1Invoker">
                                            <ul class="list-unstyled mb-0">
                                                <li>
                                                    <a class="d-flex align-items-center link-muted py-2 px-3" wire:click="edit({{ $category->id }})" href="#!">
                                                        <i class="fa fa-pencil-alt mr-2"></i> Edit
                                                    </a>
                                                </li>
                                                 <li>
                                                    <a class="d-flex align-items-center link-muted py-2 px-3" wire:click="delete({{ $category->id }})" href="#!">
                                                        <i class="fa fa-minus mr-2"></i> Remove
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div> --}}
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <!-- Modal -->
    <div wire:ignore.self class="modal fade" id="modals" tabindex="-1" role="dialog" aria-labelledby="edit-modal-label"
        aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h3 class="modal-title" id="edit-modal-label">{{$modalTitle}}
                    </h3>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form id="form_edit" method="post">
                    @csrf
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="product_name" class="form-label right">Name</label>
                                <input type="text" id="product_name" wire:model="name" class="form-control">
                                @error('name') <span class="text-danger">{{ $message }}</span>@enderror
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button wire:click.prevent="store()" type="button" class="btn btn-primary">
                            <span class="fa fa-download mr-1"></span>Save
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@push('modals')

@endpush
@push('styles')
    <link rel="stylesheet" type="text/css" href="assets/vendor/bootstrap-datepicker/css/datepicker.css" />
@endpush
@push('scripts')
    <!-- Plugins -->
    <script src="./assets/vendor/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min.js"></script>
    <!-- Initialization  -->
    <script src="./assets/js/sidebar-nav.js"></script>
    <script src="./assets/js/main.js"></script>

    <script type="text/javascript" src="assets/vendor/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
    <script type="text/javascript">
        window.livewire.on('showModal', () => {
            $('#modals').modal('show');
        });
        window.livewire.on('closeModal', () => {
            $('#modals').modal('hide');
        });
        $('.date-picker-input').datepicker({
            format: 'yyyy-mm-dd',
            autoclose: true,
            orientation: "auto",
            clearBtn : true,
            todayHighlight: true,
            templates: {
                leftArrow: '<i class="fa fa-angle-left"></i>',
                rightArrow: '<i class="fa fa-angle-right"></i>'
            }
        });
    </script>
@endpush
