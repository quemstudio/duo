<?php

use Illuminate\Support\Facades\Route;
use App\Http\Livewire\Categories;
use App\Http\Livewire\Products;
use App\Http\Livewire\Locations;
use App\Http\Livewire\Dashboards;
use App\Http\Livewire\PointOfSales;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard', function () {
    return view('dashboard');
})->name('dashboard');




Route::group(['middleware' => ['auth:sanctum', 'verified']], function() {

    Route::get('locations', Locations::class)->name('locations');
    Route::get('categories', Categories::class)->name('categories');
    Route::get('products', Products::class)->name('products');
    Route::get('dashboards', Dashboards::class)->name('dashboards');
    Route::get('pos', PointOfSales::class)->name('pos');

});
