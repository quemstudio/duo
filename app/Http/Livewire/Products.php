<?php

namespace App\Http\Livewire;
use Livewire\WithFileUploads;
use Livewire\Component;
use App\Models\Product;
use App\Models\Category;
use Livewire\WithPagination;
use Carbon\Carbon;

class Products extends Component
{
    use WithPagination;
    use WithFileUploads;

    public $product_id , $name, $in_date,$out_date,$exp_date,$sku,$buffer_level,$buy_price,$sell_price,$total,
    $brand, $supplier_id,$category_id,$location_id,$description,$picture;
    public $tittle = 'Product';
    public $modalTitle;
    protected $paginationTheme = 'bootstrap';
    public function render()
    {
        return view('livewire.products',[
            'products' => Product::paginate(15),
            'categories' => Category::all()
        ]);
    }

    public function create()
    {
        $this->resetInputFields();
        $this->modalTitle = "Add Product";
        $this->openModal();
    }

    public function store()
    {
        $this->validate([
            'name' => 'required',
            'sku' => 'required',
            'total' => 'required',
            'picture' => 'image|max:1024', // 1MB Max
        ]);
        $this->in_date == '' ? $in_date = null : $in_date = $this->in_date;
        $this->out_date == '' ? $out_date = null : $out_date = $this->out_date;
        $this->exp_date == '' ? $exp_date = null : $exp_date = $this->exp_date;
        Product::updateOrCreate(['id' => $this->product_id], [
            'name' => $this->name,
            'in_date' => $in_date,
            'out_date' =>  $out_date,
            'exp_date' =>  $exp_date,
            'sku' => $this->sku,
            'buffer_level' =>$this->buffer_level,
            'buy_price' =>$this->buy_price,
            'sell_price' =>$this->sell_price,
            'brand' =>$this->brand,
            'total' =>$this->total,
            'description' =>$this->description,
            'category_id' =>$this->category_id,
            'picture' =>$this->picture->storePublicly('picture'),
           /*  'supplier_id' =>$this->supplier_id,
            'location_id' =>$this->location_id, */
        ]);

        session()->flash('message',
            $this->product_id ? 'Product Updated Successfully.' : 'Product Created Successfully.');

        $this->closeModal();
        $this->resetInputFields();
    }

    public function edit($id)
    {
        $this->resetInputFields();
        $post = Product::findOrFail($id);
        $this->product_id   = $id;
        $this->name         = $post->name;
        $this->in_date      = $post->in_date;
        $this->out_date     = $post->out_date;
        $this->exp_date     = $post->exp_date;
        $this->sku          = $post->sku;
        $this->buffer_level = $post->buffer_level;
        $this->buy_price    = $post->buy_price;
        $this->sell_price   = $post->sell_price;
        $this->brand        = $post->brand;
        $this->total        = $post->total;
        $this->category_id        = $post->category_id;
        $this->description  = $post->description;

        $this->modalTitle = "Edit Product";
        $this->openModal();
    }

    public function delete($id)
    {
        Product::find($id)->delete();
        session()->flash('message', 'Post Deleted Successfully.');
    }
    public function openModal()
    {
        $this->emit('showModal');
        // $this->isOpen = true;
    }

    public function closeModal()
    {
        $this->emit('closeModal');
        // $this->isOpen = false;
    }

    private function resetInputFields(){
        $this->name         = null;
        $this->in_date      = null;
        $this->out_date     = null;
        $this->exp_date     = null;
        $this->sku          = null;
        $this->buffer_level = null;
        $this->buy_price    = null;
        $this->sell_price   = null;
        $this->brand        = null;
        $this->total        = null;
        $this->description  = null;
        $this->location_id  = null;
        $this->category_id  = null;
        $this->supplier_id  = null;
        $this->product_id   = null;
        $this->message = null;
    }
}
