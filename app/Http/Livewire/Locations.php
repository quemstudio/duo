<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Models\Location;
use Livewire\WithPagination;

class Locations extends Component
{
    use WithPagination;
    public $name,$address,$province_id,$city_id,$distric_id,$lat,$lng;
    public $modalTitle;
    public $tittle = 'Location';
    protected $paginationTheme = 'bootstrap';
    public function render()
    {
        return view('livewire.locations',[
            'locations' => Location::paginate(5),
        ]);
    }

    public function create()
    {
        $this->resetInputFields();
        $this->modalTitle = "Add Location";
        $this->openModal();
    }

    public function store()
    {
        $this->validate([
            'name' => 'required',
            'address' => 'required',
            'province_id' => 'required',
            'city_id' => 'required',
            'distric_id' => 'required',
            'lat' => 'required',
            'lng' => 'required'
        ]);

        Category::Create([
            'name' => $this->name,
            'address' => $this->address,
            'province_id' => $this->province_id,
            'city_id' => $this->city_id,
            'distric_id' => $this->distric_id,
            'lat' => $this->lat,
            'lng' => $this->lng,
        ]);

        session()->flash('message', 'Location Created Successfully.');

        $this->closeModal();
        $this->resetInputFields();
    }

    public function edit($id)
    {
        $this->resetInputFields();
        $post = Location::findOrFail($id);
        $this->name         = $post->name;
        $this->address      = $post->address;
        $this->province_id  = $post->province_id;
        $this->city_id      = $post->city_id;
        $this->distric_id   = $post->distric_id;
        $this->lat          = $post->lat;
        $this->lng          = $post->lng;

        $this->modalTitle = "Edit Location";
        $this->openModal();
    }

    public function delete($id)
    {
        Location::find($id)->delete();
        session()->flash('message', 'Location Deleted Successfully.');
    }
    public function openModal()
    {
        $this->emit('showModal');
        // $this->isOpen = true;
    }

    public function closeModal()
    {
        $this->emit('closeModal');
        // $this->isOpen = false;
    }

    private function resetInputFields(){
        $this->name = null;
    }
}
