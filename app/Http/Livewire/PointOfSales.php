<?php

namespace App\Http\Livewire;

use App\Models\Category;
use App\Models\Product;
use Livewire\Component;
use Livewire\WithPagination;
use App\Facades\Cart;

class PointOfSales extends Component
{
    use WithPagination;

    public $search;

    public $cartTotal = 0;
    public $cart;

    protected $updatesQueryString = ['search'];

    protected $listeners=[
        'cartAdded' => 'updateCartTotal',
        'productRemoved' => 'updateCartTotal',
    ];

    public function mount(){
//        $this->cartTotal = count(Cart::get()['products']);
        $this->cart = Cart::get();
    }

    public function render()
    {

        return view('livewire.point-of-sales',[
            'products' => $this->search === null ? Product::paginate(15) : Product::where('name','like','%'.$this->search.'%')->paginate(15),
            'categories' => Category::all()
        ]);
//        return view('livewire.point-of-sales');
    }

    public function addToCart(int $productId){

        Cart::add(Product::where('id',$productId)->first());
        $this->emitSelf('cartAdded');


    }

    public function updateCartTotal(){
//        $this->cartTotal = count(Cart::get()['products']);
        $this->cart = Cart::get();

    }

    public function removeItem(int $productId){
        Cart::remove($productId);
        $this->cart = Cart::get();
        $this->emit('productRemoved');
    }

    public function checkout(){
        Cart::clear();
        $this->emit('clearCart');
        $this->cart =  Cart::get();

    }
}
