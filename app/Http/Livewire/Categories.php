<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Models\Category;
use Livewire\WithPagination;

class Categories extends Component
{
    use WithPagination;
    public $name;
    public $modalTitle;
    public $tittle = 'Category';
    protected $paginationTheme = 'bootstrap';
    public function render()
    {
        return view('livewire.categories',[
            'categories' => Category::paginate(5),
        ]);
    }

    public function create()
    {
        $this->resetInputFields();
        $this->modalTitle = "Add Category";
        $this->openModal();
    }

    public function store()
    {
        $this->validate([
            'name' => 'required'
        ]);

        Category::Create([
            'name' => $this->name,
        ]);

        session()->flash('message', 'Product Created Successfully.');

        $this->closeModal();
        $this->resetInputFields();
    }

    public function edit($id)
    {
        $this->resetInputFields();
        $post = Category::findOrFail($id);
        $this->name         = $post->name;

        $this->modalTitle = "Edit Category";
        $this->openModal();
    }

    public function delete($id)
    {
        Category::find($id)->delete();
        session()->flash('message', 'Category Deleted Successfully.');
    }
    public function openModal()
    {
        $this->emit('showModal');
        // $this->isOpen = true;
    }

    public function closeModal()
    {
        $this->emit('closeModal');
        // $this->isOpen = false;
    }

    private function resetInputFields(){
        $this->name = null;
    }
}
