<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->date('in_date');
            $table->date('out_date');
            $table->date('exp_date');
            $table->string('sku');
            $table->string('buffer_level');
            $table->double('buy_price');
            $table->double('sell_price');
            $table->double('total');
            $table->string('brand');
            $table->integer('supplier_id');
            $table->integer('category_id');
            $table->string('description');
            $table->integer('location_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
